"""Quick test on the daily scale Poisson model


# TODO: Find reference that shows exp decrease of memory for poisson process!


For the model the drainage defines the extremes in memory estimates
But in reality the extremely different memory s series (large wetting events)
might not be realistic. it seems ~2 year series is a good enough to define 
the site memory timescale.

# NOTICE: The model assumption is that drainage happens infinitely fast! - no crossing to sfc!

# NOTICE: There seems to be linear increase in beta with increase in alpha!
R2 = 0.6885, p-value = 0.0108




"""


from __future__ import division

import numpy as np
import matplotlib.pylab as plt
import pandas as pd

from sklearn.grid_search import ParameterGrid
from eplot import eplot
import lib_cluster as lc
import lib_figs as lf

def calc_ET_yin(s, sw, s_stress, sfc, ksat, c, E_max):
    """Piecewise function for ET(s)


    """
    assert sw <= s_stress
    assert s_stress <= sfc
    if (s <= sw):
        E = 0.0        
    elif (s > sw) & (s <= s_stress):
        fact = (s - sw) / (s_stress - sw)
        E = E_max*fact

    elif (s > s_stress) & (s <= sfc):
        E = E_max
    elif (s > sfc):
        snorm = (s-sfc)/(1.0 - sfc)
        Dr = ksat*snorm**c

        # TODO: no drainage for now!
        E = E_max
        # if E >= s:
        #     E = E_max + Dr/2.0            

    elif (s >= 1.0):
        s = 1.0
        # Dr = ksat*1.0**c
        E = E_max
        
    else:
        E = 0.0

    return E

def calc_ET_loss(s, eta, s_c):
    """Calculate loss due to ET

    piecewise linear function for normalized loss rate (ET)
    under well-watered conditions

    Keyword Arguments:
    s   -- 
    eta -- 
    s_c -- 
    """
    if s < s_c:
        et_loss = (eta) * (s / s_c)
    if s >= s_c:
        et_loss = eta
    return et_loss

def calc_rand_precip(N, lamb, alpha):
    """Calculate Stochastic rainfall for N long period

    Example:
    lambda = 0.1 [#/day]
    alpha = 15 [mm]

    Keyword Arguments:
    N     -- length
    lamb  -- mean frequency of rainfall events 
    alpha -- mean depth of rainfall events
    """
    
    R = np.zeros(N)
    for ind in range(N):
        r = np.random.rand(1)
        if r < lamb:
            e = np.random.exponential(alpha, 1)
            R[ind] = e[0]
        if r >= lamb:
            R[ind] = 0
    return R

def step_soil_moisture(s, R, I, n, Zr, sw, ss, sfc, ET_max, ksat, c):
    """Calculate change in soil moisture

    Example

    Zr = 500 mm

    # sw	Sfc	s*	n	Soil Type
    # 0.14	0.28	0.21	0.46	loam
    # 0.04	0.12	0.09	0.46	loamy sand

    
    Keyword Arguments:
    s      -- soil moisture
    R      -- Rainfall
    I      -- Irrigation
    n      -- soil porosity
    Zr     -- active soil depth
    eta    -- maximum normalized ET loss rate
    s_c    -- the point of incipient stomatal closure
    s_1    -- soil moisture upper limit ~ field capacity
    ET_max -- 
    """
    
    nZr = n * Zr
    eta = ET_max/nZr
    R_norm = R/nZr
    I_norm = I/nZr # irrigation
    # ET_norm = calc_ET_loss(s, eta, ss)
    ksat = ksat/nZr
    ET_norm = calc_ET_yin(s, sw, ss, sfc, ksat, c, eta)
    
    ds_dt = R_norm + I_norm - ET_norm

    # my version
    # if (s + ds_dt < 0.0):
    #     ds_dt = -1.0*s/2.0
        # import pdb; pdb.set_trace()
    # TODO: This is not the same drainage
    
    # Leakage = percolation and runoff

    # mysirr version
    if s+ds_dt > sfc:
        LQ_norm = s + ds_dt - sfc
        ds_dt = ds_dt - LQ_norm

    return s + ds_dt


def run_one_year():
    N = 365
    R = calc_rand_precip(N, 0.1, 10)

    s = np.zeros(N)
    s[0] = 0.1

    for i, rain in enumerate(R):
        if i == 0:
            st = s[0]
        else:
            st = s[i-1]
            # s, R, Irri, n, Zr, eta, s_c, s_1, ET_max)
        s[i] = step_soil_moisture(st, rain, 0, 0.4, 300, 5, 0.21, 0.28, 5)

    df = pd.DataFrame({'rain': R, 's': s})

    return df


def run_long(lam, alpha,sw, ss,sfc, ksat, c, Emax=3.3, Rl=300, den=0.4, N=365*30):
    R = calc_rand_precip(N, lam, alpha)
    s = np.zeros(N)
    # Initial s
    s[0] = 0.1
    for i, rain in enumerate(R):
        if i == 0:
            st = s[0]
        else:
            st = s[i-1]
            # (s, R, I, n, Zr, sw, ss, sfc, ET_max, ksat, c)
        s[i] = step_soil_moisture(st, rain, 0, den, Rl, sw, ss, sfc, Emax, ksat, c)
        # if s[i] < 0.0:
        #     import pdb; pdb.set_trace()
        df = pd.DataFrame({'rain': R, 's': s})
    return df


N = 30*365
sw = 0.1
ss = 0.33
ksat = 10
c = 12
df = run_long(0.1, 15.0, sw, ss, ss/0.75, ksat, c, Emax=3.3, Rl=300, den=0.4, N=N)
memory = lc.calc_integral_scale_py(df.s.values)
print(memory)


df['s_ta'] = lc.calc_ta_stress(df.s, ss).values

colm = 'C3' # Measured
colm1 = 'C5' # Model 1
colm2 = 'C2' # Model 2
colm3 = 'C8'

def calc_persistence_beta(mak):
    tau_s_m = lc.calc_integral_scale_py(mak.s.values) # Now in days
    beta = lf.calc_dry_ip_khaled(mak.s_ta.values, tau_s_m)
    return beta


beta = calc_persistence_beta(df)



##############################################
# Calculate beta as func of Rain rate lambda #
##############################################

def set_dry(df, sw, dl):
    # dl = 250
    end = 365 - dl
    n = np.int(df.shape[0]/(1.0*dl))    
    for i in range(1,n+1):
        b = df.s[i*dl:(i*dl)+end]
        b[:] = sw
    return df    
df = set_dry(df, sw)

N = 30*365
sw = 0.1
ss = 0.33
ksat = 10
c = 12
df = run_long(0.1, 8.0, sw, ss, ss/0.75, ksat, c, Emax=3.3, Rl=300, den=0.4, N=N)
# df = set_dry(df, sw, 200)
df['s_ta'] = lc.calc_ta_stress(df.s, ss).values
memory = lc.calc_integral_scale_py(df.s.values)
print(memory)
df.s.plot()
def plot_dry_site(ax, mak, time_m, title1, colm):
    colm3 = 'C8'
    tau_s_m = lc.calc_integral_scale_py(mak.s.values) # Now in days
    lf.plot_dry_ip_khaled(ax, time_m, mak.s_ta.values, tau_s_m, 'Measured', color=colm)
    ax.legend(loc=3, frameon=False)
    ax.set(title=title1)
    ax.axvline(1.0, ls='-', alpha=0.5, color='k')
lwip = 2
fig, ax = plt.subplots(1, figsize=(8,8))
plot_dry_site(ax, df, df.index.values, 'Interarrival distribution', colm)



for dl in np.arange(100, 330, 30):
    df = run_long(0.1, 8.0, sw, ss, ss/0.75, ksat, c, Emax=3.3, Rl=300, den=0.4, N=N)
    df = set_dry(df, sw, dl)
    df['s_ta'] = lc.calc_ta_stress(df.s, ss).values
    memory = lc.calc_integral_scale_py(df.s.values)
    beta = calc_persistence_beta(df)    
    print("Memory = {:.2f}, beta = {:.2f}".format(memory, beta))

for lam in np.arange(0.05, 0.6, 0.05):
    df = run_long(lam, 15.0, sw, ss, ss/0.75, ksat, c, Emax=3.3, Rl=300, den=0.4, N=N)    
    df['s_ta'] = lc.calc_ta_stress(df.s, ss).values
    memory = lc.calc_integral_scale_py(df.s.values)
    beta = calc_persistence_beta(df)    
    print("Memory = {:.2f}, beta = {:.2f}".format(memory, beta))


for alpha in np.arange(5, 25, 5):
    df = run_long(0.1, alpha, sw, ss, ss/0.75, ksat, c, Emax=3.3, Rl=300, den=0.4, N=N)    
    df['s_ta'] = lc.calc_ta_stress(df.s, ss).values
    memory = lc.calc_integral_scale_py(df.s.values)
    beta = calc_persistence_beta(df)    
    print("Memory = {:.2f}, beta = {:.2f}".format(memory, beta))

# NOTICE: Both increase in lam and alpha increase beta    




# ax[3].set(xlabel=r'$I_{dry}/\tau$')
# for i in range(4):
#     ax[i].spines['right'].set_visible(False)
#     ax[i].get_yaxis().tick_left()
#     ax[i].spines['top'].set_visible(False)
#     ax[i].get_xaxis().tick_bottom()
#     ax[i].set_xlim(0.0007, 30)
#     ax[i].set_ylim(0.001, 200)
#     ax[i].set_ylabel(r"$PDF(I_{dry}/\tau)$")    
#     ax[i].xaxis.set_major_formatter(ticker.StrMethodFormatter("{x}"))    
# fig.subplots_adjust(hspace=0.3)
# # plt.savefig(prefix+'fig4'+iformat, bbox_inches='tight')
