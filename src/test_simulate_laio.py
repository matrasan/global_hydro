"""Quick test on the daily scale Poisson model


# TODO: Find reference that shows exp decrease of memory for poisson process!


For the model the drainage defines the extremes in memory estimates
But in reality the extremely different memory s series (large wetting events)
might not be realistic. it seems ~2 year series is a good enough to define 
the site memory timescale.

# NOTICE: There seems to be linear increase in beta with increase in alpha!
R2 = 0.6885, p-value = 0.0108

# TODO: Test for daily model!


"""


from __future__ import division

import numpy as np
import matplotlib.pylab as plt
import pandas as pd

from sklearn.grid_search import ParameterGrid

import lib_cluster as lc

def calc_ET_yin(s, sw, s_stress, sfc, ksat, c, E_max):
    """Piecewise function for ET(s)


    """
    assert sw <= s_stress
    assert s_stress <= sfc
    if (s <= sw):
        E = 0.0        
    elif (s > sw) & (s <= s_stress):
        fact = (s - sw) / (s_stress - sw)
        E = E_max*fact

    elif (s > s_stress) & (s <= sfc):
        E = E_max
    elif (s > sfc):
        snorm = (s-sfc)/(1.0 - sfc)
        Dr = ksat*snorm**c

        # TODO: no drainage for now!
        E = E_max+Dr
        # if E >= s:
        #     E = E_max + Dr/2.0            

    elif (s >= 1.0):
        s = 1.0
        Dr = ksat*1.0**c
        E = E_max+Dr
        
    else:
        E = 0.0

    return E

def calc_ET_loss(s, eta, s_c):
    """Calculate loss due to ET

    piecewise linear function for normalized loss rate (ET)
    under well-watered conditions

    Keyword Arguments:
    s   -- 
    eta -- 
    s_c -- 
    """
    if s < s_c:
        et_loss = (eta) * (s / s_c)
    if s >= s_c:
        et_loss = eta
    return et_loss

def calc_rand_precip(N, lamb, alpha):
    """Calculate Stochastic rainfall for N long period

    Example:
    lambda = 0.1 [#/day]
    alpha = 15 [mm]

    Keyword Arguments:
    N     -- length
    lamb  -- mean frequency of rainfall events 
    alpha -- mean depth of rainfall events
    """
    
    R = np.zeros(N)
    for ind in range(N):
        r = np.random.rand(1)
        if r < lamb:
            e = np.random.exponential(alpha, 1)
            R[ind] = e[0]
        if r >= lamb:
            R[ind] = 0
    return R

def step_soil_moisture(s, R, I, n, Zr, sw, ss, sfc, ET_max, ksat, c):
    """Calculate change in soil moisture

    Example

    Zr = 500 mm

    # sw	Sfc	s*	n	Soil Type
    # 0.14	0.28	0.21	0.46	loam
    # 0.04	0.12	0.09	0.46	loamy sand

    
    Keyword Arguments:
    s      -- soil moisture
    R      -- Rainfall
    I      -- Irrigation
    n      -- soil porosity
    Zr     -- active soil depth
    eta    -- maximum normalized ET loss rate
    s_c    -- the point of incipient stomatal closure
    s_1    -- soil moisture upper limit ~ field capacity
    ET_max -- 
    """
    
    nZr = n * Zr
    eta = ET_max/nZr
    R_norm = R/nZr
    I_norm = I/nZr # irrigation
    # ET_norm = calc_ET_loss(s, eta, ss)
    ksat = ksat/nZr
    ET_norm = calc_ET_yin(s, sw, ss, sfc, ksat, c, eta)
    
    ds_dt = R_norm + I_norm - ET_norm


    if (s + ds_dt < 0.0):
        ds_dt = -1.0*s/2.0
        # import pdb; pdb.set_trace()
    # TODO: This is not the same drainage
    
    # Leakage = percolation and runoff

    # if s+ds_dt > sfc:
    #     LQ_norm = s + ds_dt - sfc
    #     ds_dt = ds_dt - LQ_norm

    return s + ds_dt


def run_one_year():
    N = 365
    R = calc_rand_precip(N, 0.1, 10)

    s = np.zeros(N)
    s[0] = 0.1

    for i, rain in enumerate(R):
        if i == 0:
            st = s[0]
        else:
            st = s[i-1]
            # s, R, Irri, n, Zr, eta, s_c, s_1, ET_max)
        s[i] = step_soil_moisture(st, rain, 0, 0.4, 300, 5, 0.21, 0.28, 5)

    df = pd.DataFrame({'rain': R, 's': s})

    return df


def run_long(lam, alpha,sw, ss,sfc, ksat, c, Emax=3.3, Rl=300, den=0.4, N=365*30):
    R = calc_rand_precip(N, lam, alpha)
    s = np.zeros(N)
    # Initial s
    s[0] = 0.1
    for i, rain in enumerate(R):
        if i == 0:
            st = s[0]
        else:
            st = s[i-1]
            # (s, R, I, n, Zr, sw, ss, sfc, ET_max, ksat, c)
        s[i] = step_soil_moisture(st, rain, 0, den, Rl, sw, ss, sfc, Emax, ksat, c)
        # if s[i] < 0.0:
        #     import pdb; pdb.set_trace()
        df = pd.DataFrame({'rain': R, 's': s})
    return df






N = 5*365
sw = 0.1
ss = 0.33
ksat = 10
c = 12
df = run_long(0.05, 7.0, sw, ss, ss/0.75, ksat, c, Emax=3.3, Rl=300, den=0.4, N=N)
memory = lc.calc_integral_scale_py(df.s.values)
print(memory)


df.s.plot()


# NOTICE: There is no dry season or seasonality
# NOTICE: The memory fluctuates due to the random poisson process!
# NOTICE: You need ensemble statistics


# At daily
memory = lc.calc_integral_scale_py(df.s.values)

def run(lam, alpha):
    N = 5*365
    sw = 0.1
    ss = 0.33
    ksat = 10
    c = 12

    mem = []
    for i in range(4):
        df = run_long(lam, alpha, sw, ss, ss/0.75, ksat, c, Emax=3.3, Rl=300, den=0.4, N=N)
        memory = lc.calc_integral_scale_py(df.s.values)
        mem.append(memory)
    mem = pd.DataFrame(mem)
    return mem.mean()

param_grid = {'lam': np.arange(0.01, 0.2, 0.05).tolist(), 'alpha' : np.arange(2.0, 7.0, 1.0).tolist()}
grid = ParameterGrid(param_grid)
res = []
for params in grid:
    mem = run(params['lam'], params['alpha'])
    res.append([params['lam'], params['alpha'], mem.iloc[0]])
res = pd.DataFrame(res)

# NOTICE: The memory does not change with precipitation 
# - what if there is real drainage!
plt.scatter(res.iloc[:,0]*res.iloc[:,1], res.iloc[:,2])


def run2(Emax):
    N = 5*365
    sw = 0.1
    ss = 0.33
    ksat = 10
    c = 12

    mem = []
    for i in range(4):
        df = run_long(0.1, 7.0, sw, ss, ss/0.75, ksat, c, Emax=Emax, Rl=300, den=0.4, N=N)
        memory = lc.calc_integral_scale_py(df.s.values)
        mem.append(memory)
    mem = pd.DataFrame(mem)
    return mem.mean()

param_grid = {'Emax': np.arange(1.0, 5.0, 0.1).tolist()}
grid = ParameterGrid(param_grid)
res = []
for params in grid:
    mem = run2(params['Emax'])
    res.append([params['Emax'], mem.iloc[0]])
res = pd.DataFrame(res)
plt.scatter(res.iloc[:,0], res.iloc[:,1])
# NOTICE: Memory drops exponentially with Emax!


def run3(ss):
    N = 5*365
    sw = 0.1
    ksat = 10
    c = 12
    mem = []
    for i in range(4):
        df = run_long(0.1, 7.0, sw, ss, ss/0.75, ksat, c, Emax=3.0, Rl=300, den=0.4, N=N)
        memory = lc.calc_integral_scale_py(df.s.values)
        mem.append(memory)
    mem = pd.DataFrame(mem)
    return mem.mean()

param_grid = {'ss': np.arange(0.2, 0.4, 0.01).tolist()}
grid = ParameterGrid(param_grid)
res = []
for params in grid:
    mem = run3(params['ss'])
    res.append([params['ss'], mem.iloc[0]])
res = pd.DataFrame(res)
plt.scatter(res.iloc[:,0], res.iloc[:,1])
# NOTICE: The memomry increases linearly with s* and sfc




# R = calc_rand_precip(N, 0.1, 15)

# s = np.zeros(N)
# s[0] = 0.1

# for i, rain in enumerate(R):
#     if i == 0:
#         st = s[0]
#     else:
#         st = s[i-1]
    
#     s[i] = step_soil_moisture(st, rain, 0, 0.6, 500, 5, 0.21, 0.28, 5)
    
# df = pd.DataFrame({'rain': R, 's': s})

# print("Rain: {:.2f} mm, SM: {:.3f}".format(df.rain.sum(), df.s.mean()))

# ar = np.array(run_one_year().values)

# for i in range(1000):
#     df = run_one_year()
#     ar = np.vstack([ar, df.values])

# Histogram of soil moisture values
# plt.hist((ar[:,1]), bins=30)

s = 1.0
sw = 0.1
ss = 0.3
sfc = ss/0.75
eta = 3.3/(0.4*300)
ET_norm = calc_ET_yin(s, sw, ss, sfc, 1, 12, eta)
print(ET_norm)

N = 3*365
sw = 0.1
ss = 0.33
ksat = 1000
c = 8
df = run_long(0.1, 15.0, sw, ss, ss/0.75, ksat, c, Emax=3.3, Rl=300, den=0.4, N=N)
memory = lc.calc_integral_scale_py(df.s.values)
print(memory)
# if memory < 7.0:
#     df.s.plot(label='<7')
if memory > 17:
    df.s.plot(label='>20')
    # plt.axhline(ss/0.75)

plt.axhline(ss/0.75)

plt.legend()
# NOTICE: The memory estimates vary a lot with the current drainage which can introduce spikes
# 
