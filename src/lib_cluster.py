"""Library to do spectra, integral scale, interpulse etc. for soil moisture

"""

from __future__ import division




import numpy as np
import pandas as pd
import matplotlib.pylab as plt
import scipy.signal as sg
import powerlaw

from configobj import ConfigObj

from itertools import groupby


def make_ycol(df, years, fr='H'):
    df['ycol'] = np.nan
    for k, yr in enumerate(years):
        if k == 0:
            df.ycol.ix[df.index[0]:pd.to_datetime(str(yr+1)+'0901')] = int(yr)
        else:
            ind = pd.date_range(str(yr)+'0901', str(yr+1)+'0901', freq=fr)[0:-1]
            try:
                df.ycol.ix[ind] = int(yr)
            except KeyError:
                try:
                    df.ycol.ix[pd.to_datetime(str(yr)+'0901'):] = int(yr)

                except KeyError:
                    pass
    return df.ycol


# Maktau
def calc_lw2(T,RH, z_elev):
    """Rigden Salvucci

    T = Celsius
    RH = %
    z_elev   = 1480 m  (welge)
    """
    # T in Celsius
    es = 0.6108 * np.exp(17.27 * T / (T + 237.3))
    e = RH / 100.0 * es 
    e = e * 1000
    
    Tk = T + 273.15
    # e in Pascal, T in Kelvin, z_elev in m
    r_ld = 0.045*e + 2.18*Tk - 0.0069*z_elev - 351.2

    return r_ld

def calculate_netrad(global_up, airt, RH):
    """Calculate net radiation for Maktau
    """
    # Assuming 30min step - Is ok  for hour?
    emis = 0.98
    lw_d = calc_lw2(airt, RH, 1055)
    lw_d  = lw_d  - ((1-emis)*lw_d)
    sg = 5.67e-8 #W/m2/K4
    lw_u = sg * 0.95*(airt+273.15)**4
    net = lw_d - lw_u

    # Bare measured 0.12
    # Previous 0.26 for grass
    # albedo = 0.26

    albedo = 0.12
    rg = (1-albedo)*global_up + net
    return rg


def flag_gaps(ser):
    """Flag gaps of boolean series with the size of the gap
    
    Keyword Arguments:
    ser -- Boolean Series
    """
    x = ser.copy()
    x = x.values.astype(float)
    x[x==0] = np.nan

    y = np.zeros_like(x, dtype=int)
    y[np.where(np.isnan(x))] = 1 # Locate where the array is nan


    z = []
    for a, b in groupby(y, lambda x: x == 0):
        if a: # Where the value is 0, simply append to the list
            z.extend(list(b))
        else: # Where the value is one, replace 1 with the number of sequential 1's
            l = len(list(b))
            z.extend([l]*l)    
    z = np.array(z)
    return z

def make_sub(df):
    df['subp'] = np.nan
    df['subp'][(df.index.month == 9) | (df.index.month == 10) | (df.index.month == 11)] = 1
    df['subp'][(df.index.month == 12) | (df.index.month == 1) | (df.index.month == 2)] = 2
    df['subp'][(df.index.month == 3) | (df.index.month == 4) | (df.index.month == 5)] = 3
    df['subp'][(df.index.month == 6) | (df.index.month == 7) | (df.index.month == 8)] = 4
    return df


def get_site_params(path, site):
    """Get site parameters: intercept, rootzone, porosity
    """
    conf = ConfigObj('../../images/memory/Site_parameters.txt')[site]
    int_p = float(conf['p_interception'])
    Rl = float(conf['Rl'])
    n = float(conf['porosity'])
    ss = float(conf['ss'])

    return int_p, Rl, n, ss


def calc_interpulse(t, x):
    x = x - x.mean()
    n = x.shape[0]
    u1 = x[0:n - 1]
    u2 = x[1:n]
    pr = u1 * u2
    # np.nonzero(a>0.5)
    Dur = t[np.where(pr < 0)]
    return np.diff(Dur)

def calc_ta_stress(s, s_stress):
    """TA around the mean

    # NOTICE: s - s* should not be exactly zero!!!
    """
    s = s - s_stress
    ta = 0.5 * (s / np.abs(s) + 1)
    return ta


def ta_transform(x):
    """TA around the mean
    """
    x = x - np.mean(x)
    ta = 0.5 * (x / np.abs(x) + 1)
    return ta

def calc_ss_TA(x, ss):
    """TA around the ss treshold
    """
    x = x - ss
    ta = 0.5 * (x / np.abs(x) + 1)
    return ta

def calc_cluster_exponent_matlab(t):
    """Calculate clustering exponent

    Sreenivasan(2006) Eq (2)

    maxscale = np.floor(np.log2(fog.shape[0]/16.))
    """
    import matlab.engine
    eng = matlab.engine.start_matlab("-nodisplay")

    eng.workspace['T'] = matlab.double(t.tolist())

    code = "compute_cluster_exponent(T);"
    std_den, mean_den, wind = eng.eval(code, nargout=3)

    std_den = np.array(std_den).squeeze()
    mean_den = np.array(mean_den).squeeze()
    wind = np.array(wind).squeeze()

    return std_den, mean_den, wind


def calc_ns_spectra_python(var, var_ta, fs, nw):
    """Python version
    """
    var_ndev = (var - var.mean()) / (var.std())

    f, p = sg.welch(var_ndev, fs=fs, nperseg=var.shape[0]/nw, detrend=False)
    ft, pt = sg.welch(var_ta, fs=fs, nperseg=var.shape[0]/nw, detrend=False)

    return f, p, pt


def calc_spectra(t, t_ta, dt):
    n = t.shape[0]
    nw = 16
    # nw = 4
    nwind = np.floor(n / nw)
    
    print("Welch method window size: {:.3f} %".format(nwind / t.shape[0] * 100))

    _, pxx_ta = sg.welch(t_ta, nperseg=nwind)
    _, pxx_t = sg.welch(t, nperseg=nwind)

    nn = pxx_ta.shape[0]

    freq = (1 / dt) * np.arange(1, nn + 1) / (2 * nn)

    sig_spec = np.sum(pxx_t) / nn
    sig_time = np.var(t)

    return freq, pxx_t, pxx_ta, sig_spec, sig_time

def calc_spectra_matlab(t, t_ta, dt, nw=4.0):
    """Calculate spectra using Matlab psd

    nw = 4.0
    nwind = floor(n/nw);
    ET_TA = psd(T_TA,nwind)
    freq= (1/dt)*[1:1:nn]'/(2*nn) 

    """
    import matlab.engine
    eng = matlab.engine.start_matlab("-nodisplay")

    eng.workspace['T'] = matlab.double(t.tolist())
    eng.workspace['T_TA'] = matlab.double(t_ta.tolist())
    eng.workspace['dt'] = matlab.double([dt])
    eng.workspace['nw'] = matlab.double([nw])

    code = "compute_spectra(T, T_TA, dt, nw);"
    freq, et, et_ta, sig_spec, sig_time = eng.eval(code, nargout=5)

    freq = np.array(freq).squeeze()
    et = np.array(et).squeeze()
    et_ta = np.array(et_ta).squeeze()

    return freq, et, et_ta, sig_spec, sig_time

def calc_spectra_pwelch_matlab(t, t_ta, fs, nw=4.0):
    """Calculate spectra using Matlab pwelch - USE THIS version

    nw = 4.0

    """
    import matlab.engine
    eng = matlab.engine.start_matlab("-nodisplay")

    eng.workspace['T'] = matlab.double(t.tolist())
    eng.workspace['T_TA'] = matlab.double(t_ta.tolist())
    eng.workspace['fs'] = matlab.double([fs])
    eng.workspace['nw'] = matlab.double([nw])

    code = "compute_spectra_pwelch(T, T_TA, fs, nw);"
    freq, et, et_ta, sig_spec, sig_time = eng.eval(code, nargout=5)

    freq = np.array(freq).squeeze()
    et = np.array(et).squeeze()
    et_ta = np.array(et_ta).squeeze()

    return freq, et, et_ta, sig_spec, sig_time


def calc_log_fit(x, y):
    
    def yfit(x):
        return np.exp(poly(np.log(x)))

    # Filter zeros
    cond = ~(y <= 0)
    y = y[cond]
    x = x[cond]

    lx = np.log(x)
    ly = np.log(y)

    cond2 = ~np.isinf(lx)
    ly = ly[cond2]
    lx = lx[cond2]    

    coeffs = np.polyfit(lx, ly, 1)
    poly = np.poly1d(coeffs)
    yf = yfit(x)
    
    return x, yf, coeffs


def calc_integral_scale(x):
    """Calculate the integral scale using the zero-crossing of the autocorr

    # NOTICE: THIS can fail and return 1
    # NOTICE: Assuming x size is power of 2
    Uses FFT method. 

    Keyword Arguments:
    x -- Must be integer power of 2 in length
    """
    
    import matlab.engine
    eng = matlab.engine.start_matlab("-nodisplay")
    
    eng.workspace['x'] = matlab.double(x.tolist())
    
    code = "integral_scale(x);"
    I_s = eng.eval(code, nargout=1)

    if np.array(I_s).squeeze().size == 0:
        I_s = 1    
    return I_s



def autocorrelation3(x):
    xp = (x - np.mean(x))/np.std(x)
    result = np.correlate(xp, xp, mode='full')
    return result[int(result.size/2):]/len(xp)


def calc_integral_scale_py(x):
    """Calculate integral 

    """
    cr1 = autocorrelation3(x)
    mm = np.argmax(cr1<0)
    Is = np.trapz(cr1[:mm])
    return Is

def calc_autocorr_matlab(a, maxlag):
    """Calculate autocorrelation up to maxlag

    """
    import matlab.engine
    eng = matlab.engine.start_matlab("-nodisplay")

    eng.workspace['a'] = matlab.double(a.tolist())
    eng.workspace['maxlag'] = matlab.double([maxlag])

    code = "autocorr(a,maxlag);"
    ac = eng.eval(code, nargout=1)
    ac = np.array(ac).squeeze()
    return ac


def plot_autocorr_func(ax, ac_s):    
    ax.plot(ac_s)
    ax.axhline(0,ls='--', alpha=0.5, color='k')
    ax.set_xlabel("Lag [hour]")
    ax.set_ylabel("Autocorrelation")

def plot_spectra(ax, freq, s_e, s_ta_e, rain_e, rain_ta_e, tau_s):
    """Plot spectra for soil moisture and rain
    
    """

    xx, yy, coeff = calc_log_fit(freq, s_e)
    xx2, yy2, coeff2 = calc_log_fit(freq, s_ta_e)

    xxr, yyr, coeffr = calc_log_fit(freq, rain_e)
    xx2r, yy2r, coeff2r = calc_log_fit(freq, rain_ta_e)

    ax[0].loglog(freq, s_e, '-', label='E')
    ax[0].loglog(xx, yy, '-', label='E_fit')
    ax[0].set_ylabel("E_ns_soil_moisture(f)")
    ax[0].set_xlabel("f (1/hour)")
    ax[0].set_title("alpha = {:.2f}".format(coeff[0]))
    
    ax[2].loglog(freq, s_ta_e, '-', label='E_TA')
    ax[2].loglog(xx2, yy2, '-', label='E_TA_fit')
    ax[2].set_ylabel("E_soil_moisture_TA(f)")
    ax[2].set_xlabel("f (1/hour)")
    ax[2].set_title("alpha = {:.2f}".format(coeff2[0]))

    ax[1].loglog(freq, rain_e, '-', label='E')
    ax[1].loglog(xxr, yyr, '-', label='E_fit')
    ax[1].set_ylabel("E_ns_rain(f)")
    ax[1].set_xlabel("f (1/hour)")
    ax[1].set_title("alpha = {:.2f}".format(coeffr[0]))

    ax[3].loglog(freq, rain_ta_e, '-', label='E_TA')
    ax[3].loglog(xx2r, yy2r, '-', label='E_TA_fit')
    ax[3].set_ylabel("E_rain_TA(f)")
    ax[3].set_xlabel("f (1/hour)")
    ax[3].set_title("alpha = {:.2f}".format(coeff2r[0]))

    ax[0].axvline(1/tau_s,ls='-', alpha=0.5, color='r')
    ax[2].axvline(1/tau_s,ls='-', alpha=0.5, color='r')
    
    for i in ax:
        i.axvline(1/12.,ls='--', alpha=0.5, color='k')
        i.axvline(1/24.,ls='--', alpha=0.5, color='k')
        i.axvline(1/720.,ls='--', alpha=0.5, color='k')
        i.axvline(1/8760.,ls='--', alpha=0.5, color='k')


def plot_run(ax, df, L, wm, w_measured):
    

    ln1 = ax[1].plot(w_measured, label='Measured w', zorder=1)
    ax[0].plot(df.precip.values, label='Measured P')
    ax[0].legend()
    ax[2].plot(L, label='Modeled Loss')
    ax[2].legend()
    ax2 = ax[1].twinx()
    ln2 = ax2.plot(wm, label='Modeled w', c='b',zorder=0, alpha=0.8)
    lns = ln1+ln2
    labs = [l.get_label() for l in lns]
    ax2.legend(lns, labs, loc=0)
    ylims = np.array(ax[1].get_ylim()+ax2.get_ylim())
    _,_= ax[1].set_ylim(ylims.min(), ylims.max())
    _,_= ax2.set_ylim(ylims.min(), ylims.max())

def plot_model_output(ax, precip, wm, s, L):
    """Plot loss function (L),  and soil water (s) 

    """
    
    # title = "{} -  alpha = {}, Rl = {}, MODEL = {}".format(site, alpha, Rl, model)
    # fig.suptitle(title)
    ln1 = ax[1].plot(s, label='Measured s', zorder=1)
    ax[0].plot(precip, label='Measured P')
    ax[0].legend()
    ax[0].set_ylabel('[mm]')
    ax[2].plot(L, label='Modeled Loss')
    ax[2].legend()
    ax[2].set_ylabel('[mm]')
    ax2 = ax[1].twinx()
    ln2 = ax2.plot(wm, label='Modeled s', c='b',zorder=0, alpha=0.8)
    lns = ln1+ln2
    labs = [l.get_label() for l in lns]
    ax2.legend(lns, labs, loc=0)
    ylims = np.array(ax[1].get_ylim()+ax2.get_ylim())
    _,_= ax[1].set_ylim(ylims.min(), ylims.max())
    _,_= ax2.set_ylim(ylims.min(), ylims.max())
    ax[1].set_ylabel('[mm]')
    ax2.set_ylabel('[mm]')

    
def plot_ip_pdf(ax, time, s_ta, precip_ta, tau_s):
    """Plot the pdf of interpulse periods for soil moisture

    # NOTICE: 
    """

    # # NOTICE: The inputs must be numpy arrays
    ip_ns = calc_interpulse(time, s_ta) / tau_s
    # ip_rain = calc_interpulse(time, precip_ta)
    
    fit = powerlaw.Fit(ip_ns)

    # Compare power to lognormal - powerlaw better
    # fit.distribution_compare('power_law', 'lognormal', normalized_ratio = True)
    #  This number will be positive if the data is more
    # likely in the first distribution, and negative if the data is more likely
    # in the second distribution. 

    # Fit pdf
    axip = fit.plot_pdf(ax=ax, color='b', linewidth=2)

    # Plot the pdf of the data

    x, y = powerlaw.pdf(ip_ns)
    # x = bin_edges : array
    # y = probabilities : array (+1 larger)
    ind = y>0
    y = y[ind]
    x = x[:-1]
    x = x[ind]

    axip.scatter(x, y, color='r', s=10)
    axip.axvline(1.0,ls='-', alpha=0.5, color='r')
    axip.set_ylabel("pdf(Is/tau)")
    axip.set_xlabel("Is/tau")

        
def plot_specral_analysis(wm, df, L, dt, Rl, n, prefix, img_path):
    """Do complete spectral analysis and plot for soil moisture

    Parameters:
    wm -- soil moisture [np.array]
    prefix -- Prefix for all the plot names
    img_path -- Path to image folder
    """
    
    path = img_path+"/"+prefix

    fs = 1 / dt
    time = np.arange(wm.shape[0]) * dt

    # TA'wm
    s_ta = ta_transform(wm)

    rain = df.precip
    rain_ta = df.precip.copy()
    rain_ta[rain_ta > 0.0] = 1.

    # Calculate memory timescale
    tau_s = calc_integral_scale(wm)

    # #############################
    # # Plot autocorrelation of wm #
    # #############################
    # ac_s = calc_autocorr_matlab(wm, 100*24)
    # fig, ax = plt.subplots(1, figsize=(10, 10))
    # plot_autocorr_func(ax, ac_s)
    # plt.savefig(path+'autocorr_s.png')

    #####################
    # plot model output #
    #####################

    fig, ax = plt.subplots(3, figsize=(14,10), sharex=True)

    # NOTICE: Make all inputs numpy
    w_measured = df.s.values * n * Rl
    
    plot_run(ax, df, L, wm, w_measured)
    
    # plot_model_output(ax, rain.values, wm, s.values, L)
    plt.savefig(path+'time_series.png')

    
    #############################
    # Plot spectra using pwelch #
    #############################
    

    # Normalize so that you can compare different sites
    s_ndev = (wm - wm.mean()) / (wm.std())
    p_ndev = (rain - rain.mean()) / (rain.std())

    nw = 2.0
    freq, s_e, s_ta_e, _,_ = calc_spectra_pwelch_matlab(s_ndev, df.s_ta, fs, nw=nw)
    _, rain_e, rain_ta_e, _,_ = calc_spectra_pwelch_matlab(p_ndev, rain_ta, fs, nw=nw)

    fig, ax = plt.subplots(2,2, figsize=(12,12))
    ax = ax.flatten()
    fig.suptitle("Memory {:.2f} days".format(tau_s/24))
    plot_spectra(ax, freq, s_e, s_ta_e, rain_e, rain_ta_e, tau_s)
    plt.savefig(path+'s_rain_spectra.png')

    #######################
    # Plot interpulse pdf #
    #######################

    fig, ax = plt.subplots(1, figsize=(10,10))
    plot_ip_pdf(ax, time, s_ta, rain_ta, tau_s)
    plt.savefig(path+'pdf_normalized_persistence.png')

def plot_specral_analysis_meas(df, dt, nw, Rl, prefix):
    """Do complete spectral analysis for the measured data

    """

    fs = 1 / dt
    time = np.arange(df.s.shape[0]) * dt

    tau_s = calc_integral_scale(df.s) # in hours


    # Soil moisture Tau : 1873.55 hours
    print("Soil moisture Tau : {0:.2f} hours".format(tau_s))

    ac_s = calc_autocorr_matlab(df.s, 100*24)
    fig, ax = plt.subplots(1, figsize=(10, 10))
    plot_autocorr_func(ax, ac_s)
    plt.savefig(prefix+'autocorr_s.png')

    ########################
    # Plot s and P spectra #
    ########################

    # ~4.4 years of data - nw = 4

    # Taro: ~4 years of data - nw = 2
    s_ndev = (df.s - df.s.mean()) / (df.s.std())
    p_ndev = (df.precip - df.precip.mean()) / (df.precip.std())


    freq, s_e, s_ta_e, _,_ = calc_spectra_pwelch_matlab(s_ndev, df.s_ta, fs, nw=nw)
    _, rain_e, rain_ta_e, _,_ = calc_spectra_pwelch_matlab(p_ndev, df.rain_ta, fs, nw=nw)

    fig, ax = plt.subplots(2,2, figsize=(12,12))
    ax = ax.flatten()
    fig.suptitle("Memory {:.2f} days".format(tau_s/24))
    plot_spectra(ax, freq, s_e, s_ta_e, rain_e, rain_ta_e, tau_s)
    plt.savefig(prefix+'s_rain_spectra.png')

    #######################
    # Plot interpulse pdf #
    #######################

    fig, ax = plt.subplots(1, figsize=(10,10))
    plot_ip_pdf(ax, time, df.s_ta.values, df.rain_ta.values, tau_s)
    plt.savefig(prefix+'pdf_normalized_persistence.png')

def make_data():
    """For each site
    """ 
    mak = pd.read_pickle('../../data_output/memory/maktau_meteo.pickle')
    _, Rl_m, n_m, _ = get_site_params('../../images/memory/Site_parameters.txt', 'maktau')

    # TODO: Diff subs in Maktau?
    mak = make_sub(mak)

    years = mak.index.year.unique().tolist()
    mak['ycol'] = make_ycol(mak, years)
    mak['ycol'] = mak.ycol.astype(int)


    # NOTICE: High drainage - Is the params correct?
    params_mak = {
        'Rl': Rl_m,
        'n': n_m, # porosity
        'p_int': 0.85,
        'E_max': 3.1/24., # Welgegund!
        'sw': 0.08, # Put close to observed s_min
        's_stress': 0.25, # Set to same as Welgegund!
        'ksat': 50./24.*10, # 100cm/day
        'c': 2*4.9+3,
        'sfc': 0.56
    }

    # mak['s'] = mak.wc_avg/params_mak['n']

    # TODO: Test 10, 30 measurement!
    mak['s'] = mak[['wc10', 'wc30']].mean(axis=1)/params_mak['n']

    kru = pd.read_pickle("../../data/Kruger_2017_Gregor_Feig/Skukuza_gf_final.pickle")
    kru = kru.resample("H").mean()

    # Final
    pfill = pd.read_pickle("../../data_output/Kruger_exp_pfill.pickle")    

    kru.precip = pfill.values

    assert kru.s.isnull().sum() == 0
    assert kru.precip.isnull().sum() == 0

    kru = kru['20150223':]

    kru = make_sub(kru)

    years = [2014]+kru.index.year.unique().tolist()
    kru['ycol'] = make_ycol(kru, years)
    kru['ycol'] = kru.ycol.astype(int)

    _, Rl_k, n_k, _ = get_site_params('../../images/memory/Site_parameters.txt', 'skukuza')

    # s* Must be at 3 decimals to not have exact match with sm measurement


    params_kru = {
        'Rl': Rl_k,
        'n': n_k,
        'p_int': 0.92, # S vs P
        'E_max': 3.3/24., # From figure the fig 1
        'sw': 0.12, # From time series
        's_stress': 0.251, # Fig 0.25
        'ksat': 100*10./24., # Set same for all sites?
        'c': 2*4.9+3, # c = 2*b+3, Laio
        'sfc': 0.4 # Read from the time series
    }

    # NOTICE: wc2c has issues
    kru['wc_avg'] = kru[['wc1c', 'wc3c']].mean(axis=1)
    kru['s'] = kru.wc_avg/params_kru['n']

    # WELGE
    # path1 = '../../../Evaporation/data/draft/final/met_flux_6_yr_final.pickle'
    # df = pd.read_pickle(path1)
    # df = make_sub(df)

    wel = pd.read_pickle('../../data_output/memory/welge_meteo.pickle')
    _, Rl_w, n_w, _ = get_site_params('../../images/memory/Site_parameters.txt', 'welgegund')
    #  Index Bug - something with pickle
    wel.index = pd.Index(wel.index,freq=wel.index.freqstr)


    # SELECT PERIOD
    # wel = wel[:'20130501']

    params_wel = {
        'Rl': Rl_w,
        'n': n_w, # porosity
        'p_int': 1.0,
        'E_max': 3.1/24., # MODEL 2: From fitted EC data
        # 'sw': 0.11,        # Laio
        # 's_stress': 0.31,  # Laio
        'sw': 0.11, # Laio=0.11
        's_stress': 0.35, # From EC
        'ksat': 80./24.*10, # Scholes: 100 cm/day
        'c': 2*4.38+3,
        'sfc': 0.52
    }

    wel['s'] = wel.wc_avg/params_wel['n']
    wel = make_sub(wel)

    years = [x+2011 for x in range(7)]
    wel['ycol'] = make_ycol(wel, years)
    wel['ycol'] = wel.ycol.astype(int)


    return wel, mak, kru

def make_mongu():
    mon = pd.read_pickle('../../data/Zambia_Lutz/Zambia_Lutz.pickle')

    per = slice('2007-09-15', '2008-06-01')
    mon = mon[per]
    # years = [x+2007 for x in range(2)]
    # mon['ycol'] = make_ycol(mon, years)

    mcols = mon.filter(regex='sm').columns.tolist()

    # mon[['sm005a', 'precip']].plot(subplots=True)
    mon = make_sub(mon)

    years = mon.index.year.unique().tolist()
    mon['ycol'] = make_ycol(mon, years)
    mon['ycol'] = mon.ycol.astype(int)

    # mon.sm005a.plot()
    ###############
    # Gapfilling  #
    ###############

    mon.precip[mon.precip.isnull()] = 0.0

    for c in mcols:
        mon[c][mon[c] < -7000] = np.nan
        # Linear int 25 days
        # mon[c] = mon[c].interpolate(method='linear', limit=82*48)
        mon[c] = mon[c].fillna(method='pad', limit=82*48)
        mon[c] = mon[c]/100.0

    ##########
    # Mongu #
    ##########

    site = "Mongu"
    # print("\n{}".format(site))

    _, Rl_z, n_z, _ = get_site_params('../../images/memory/Site_parameters.txt', 'mongu')

    mon = mon.resample("H").mean()
    mon['precip'] = mon.precip.resample("H").sum()

    rain_ta = mon.precip.copy()
    rain_ta[rain_ta > 0.0] = 1.
    mon['rain_ta'] = rain_ta

    # NOTICE: High drainage - Is the params correct?
    params_mon = {
        'Rl': Rl_z,
        'n': n_z, # porosity
        'p_int': 1.0,
        'E_max': 2.8/24., # Fig
        'sw': 0.07, # Put close to observed s_min
        's_stress': 0.17, # Fig - Very low!!
        'ksat': 50./24.*10, # 100cm/day
        'c': 2*4.9+3,
        'sfc': 0.56
    }


    # NOTICE: a=OPen spaces - memory 45 days
    mon['s'] = mon[['sm005a', 'sm030a', 'sm050a']].mean(axis=1)/params_mon['n']
    mon['sb'] = mon[['sm005b', 'sm030b', 'sm050b']].mean(axis=1)/params_mon['n']

    return mon

def calc_pos(s, n, Rl):
    wm = s * n * Rl    
    dwm = wm.diff()
    dwm[dwm <= 0.0] = 0.0
    # wm = dwm[dwm > 0.0]
    return dwm

def get_rain_events(df):
    # Calculate the rain flag with all data
    gap_length = df.precip > 0.0
    df['gap_length'] = flag_gaps(gap_length)

    df['rflag_filled'] = df.precip > 0.0

    # gap_length = The size of the gap between rain events

    ######################################################
    # Flag the rain events which less than 3 hours apart #
    ######################################################

    # rflag_filled = True for rain and < 3 hour gaps
    # rflag_cat = Categorical for each rain event

    for idx, window in df.groupby(df.index.date):
        ind = window.rflag_filled[(window.gap_length <=5) & (window.gap_length > 0)].index    
        if not df.rflag_filled.loc[ind].empty:
            df.rflag_filled.loc[ind] = True

    # Mark each step change with running number
    df['rflag_cat'] = (df.rflag_filled.shift(1) != df.rflag_filled).astype(int).cumsum()
    # Set no rain periods to 0
    df.loc[df.rflag_filled == False, 'rflag_cat'] = 0
    df['rflag_cat'] = pd.Categorical(df['rflag_cat'])
    # Rename categories to running number
    df.rflag_cat = df.rflag_cat.cat.rename_categories(range(df.rflag_cat.cat.categories.shape[0]))
    return df
