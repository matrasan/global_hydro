"""Library for calculating water balance


"""
from __future__ import division

import numpy as np
# import pandas as pd


def calc_ET_yin(s, sw, s_stress, sfc, ksat, c, E_max):
    """Piecewise function for ET(s)

    """
    assert sw <= s_stress
    assert s_stress <= sfc
    if (s <= sw):
        E = 0.0        
    elif (s > sw) & (s <= s_stress):
        fact = (s - sw) / (s_stress - sw)
        E = E_max*fact

    elif (s > s_stress) & (s <= sfc):
        E = E_max
    elif (s > sfc):
        snorm = (s-sfc)/(1.0 - sfc)
        Dr = ksat*snorm**c

        # TODO: no drainage for now!
        E = E_max
        # if E >= s:
        #     E = E_max + Dr/2.0            

    elif (s >= 1.0):
        s = 1.0
        # Dr = ksat*1.0**c
        E = E_max
        
    else:
        E = 0.0

    return E

def calc_ET_loss(s, eta, s_c):
    """Calculate loss due to ET

    piecewise linear function for normalized loss rate (ET)
    under well-watered conditions

    Keyword Arguments:
    s   -- 
    eta -- 
    s_c -- 
    """
    if s < s_c:
        et_loss = (eta) * (s / s_c)
    if s >= s_c:
        et_loss = eta
    return et_loss

def calc_rand_precip(N, lamb, alpha):
    """Calculate Stochastic rainfall for N long period

    Example:
    lambda = 0.1 [#/day]
    alpha = 15 [mm]

    Keyword Arguments:
    N     -- length
    lamb  -- mean frequency of rainfall events 
    alpha -- mean depth of rainfall events
    """
    
    R = np.zeros(N)
    for ind in range(N):
        r = np.random.rand(1)
        if r < lamb:
            e = np.random.exponential(alpha, 1)
            R[ind] = e[0]
        if r >= lamb:
            R[ind] = 0
    return R

def step_soil_moisture(s, R, I, n, Zr, sw, ss, sfc, ET_max, ksat, c):
    """Calculate change in soil moisture

    Example

    Zr = 500 mm

    # sw	Sfc	s*	n	Soil Type
    # 0.14	0.28	0.21	0.46	loam
    # 0.04	0.12	0.09	0.46	loamy sand

    
    Keyword Arguments:
    s      -- soil moisture
    R      -- Rainfall
    I      -- Irrigation
    n      -- soil porosity
    Zr     -- active soil depth
    eta    -- maximum normalized ET loss rate
    s_c    -- the point of incipient stomatal closure
    s_1    -- soil moisture upper limit ~ field capacity
    ET_max -- 
    """
    
    nZr = n * Zr
    eta = ET_max/nZr
    R_norm = R/nZr
    I_norm = I/nZr # irrigation
    # ET_norm = calc_ET_loss(s, eta, ss)
    ksat = ksat/nZr
    ET_norm = calc_ET_yin(s, sw, ss, sfc, ksat, c, eta)
    
    ds_dt = R_norm + I_norm - ET_norm

    # my version
    # if (s + ds_dt < 0.0):
    #     ds_dt = -1.0*s/2.0
        # import pdb; pdb.set_trace()
    # TODO: This is not the same drainage
    
    # Leakage = percolation and runoff

    # mysirr version
    if s+ds_dt > sfc:
        LQ_norm = s + ds_dt - sfc
        ds_dt = ds_dt - LQ_norm

    return s + ds_dt


def run_one_year():
    N = 365
    R = calc_rand_precip(N, 0.1, 10)

    s = np.zeros(N)
    s[0] = 0.1

    for i, rain in enumerate(R):
        if i == 0:
            st = s[0]
        else:
            st = s[i-1]
            # s, R, Irri, n, Zr, eta, s_c, s_1, ET_max)
        s[i] = step_soil_moisture(st, rain, 0, 0.4, 300, 5, 0.21, 0.28, 5)

    df = pd.DataFrame({'rain': R, 's': s})

    return df


def run_long(lam, alpha,sw, ss,sfc, ksat, c, Emax=3.3, Rl=300, den=0.4, N=365*30):
    R = calc_rand_precip(N, lam, alpha)
    s = np.zeros(N)
    # Initial s
    s[0] = 0.1
    for i, rain in enumerate(R):
        if i == 0:
            st = s[0]
        else:
            st = s[i-1]
            # (s, R, I, n, Zr, sw, ss, sfc, ET_max, ksat, c)
        s[i] = step_soil_moisture(st, rain, 0, den, Rl, sw, ss, sfc, Emax, ksat, c)
        # if s[i] < 0.0:
        #     import pdb; pdb.set_trace()
        df = pd.DataFrame({'rain': R, 's': s})
    return df
