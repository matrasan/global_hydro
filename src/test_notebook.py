"""Water balance with no drainage

Code for notebook


"""


from __future__ import division

import numpy as np
import matplotlib.pylab as plt
import pandas as pd

import lib_water as lw

# from sklearn.grid_search import ParameterGrid
# from eplot import eplot
# import lib_cluster as lc
# import lib_figs as lf


N = 5*365
sw = 0.1
ss = 0.33
sfc = ss/0.75

ksat = 10
c = 12

# Mean daily rainfall rate (storm/day)
lam= 0.1
# Mean daily rainfall amount (mm)
alpha = 8.0
 
# Generate daily rainfall
R = lw.calc_rand_precip(N, lam, alpha)

s = np.zeros(N)
# Initial soil moisture
s[0] = 0.1

for i, rain in enumerate(R):
    if i == 0:
        st = s[0]
    else:
        st = s[i-1]
        # (s, R, I, n, Zr, sw, ss, sfc, ET_max, ksat, c)
        # step_soil_moisture(s, R, I, n, Zr, sw, ss, sfc, ET_max, ksat, c)
    s[i] = lw.step_soil_moisture(st, rain, 0, 0.4, 300, sw, ss, sfc, 3.3, ksat, c)
    df = pd.DataFrame({'rain': R, 's': s})


# df = lw.run_long(0.1, 15.0, sw, ss, ss/0.75, ksat, c, Emax=3.3, Rl=300, den=0.4, N=N)
# memory = lc.calc_integral_scale_py(df.s.values)
# print(memory)


fig, ax = plt.subplots(2, figsize=(8,8))
df.rain.plot(ax=ax[0])
df.s.plot(ax=ax[1])
ax[0].set(ylabel="Rainfall")
ax[1].set(ylabel="Soil moisture")
