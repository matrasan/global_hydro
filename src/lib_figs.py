"""Lib for plotting functions



"""
from __future__ import division

import numpy as np
import matplotlib.pylab as plt
import pandas as pd
import scipy.signal as sg

from matplotlib.offsetbox import AnchoredText
from sklearn.metrics import r2_score

import matplotlib.dates as mdates

# import lmfit as lm

import my_powerlaw as mp
from matplotlib import rc
import warnings
from matplotlib import ticker


def calc_ta_stress(s, s_stress):
    """TA around the mean

    # NOTICE: s - s* should not be exactly zero!!!
    """
    s = s - s_stress
    ta = 0.5 * (s / np.abs(s) + 1)
    return ta

def autocorrelation3(x):
    xp = (x - np.mean(x))/np.std(x)
    result = np.correlate(xp, xp, mode='full')
    return result[int(result.size/2):]/len(xp)


def calc_integral_scale_py(x):
    """Calculate integral 

    """
    cr1 = autocorrelation3(x)
    mm = np.argmax(cr1<0)
    Is = np.trapz(cr1[:mm])
    return Is

def calc_ns_spectra_python(var, var_ta, fs, nw):
    """Python version
    """
    var_ndev = (var - var.mean()) / (var.std())

    f, p = sg.welch(var_ndev, fs=fs, nperseg=var.shape[0]/nw, detrend=False)
    ft, pt = sg.welch(var_ta, fs=fs, nperseg=var.shape[0]/nw, detrend=False)

    return f, p, pt

def calc_log_fit(x, y, xlow):
    
    def yfit(x):
        return np.exp(poly(np.log(x)))

    cond = (x > xlow)
    # cond = (x > xlow) & (x < 1/24.)

    x = x[cond]
    y = y[cond]

    # Filter zeros
    cond = ~(y <= 0)
    y = y[cond]
    x = x[cond]

    lx = np.log(x)
    ly = np.log(y)

    cond2 = ~np.isinf(lx)
    ly = ly[cond2]
    lx = lx[cond2]    

    coeffs = np.polyfit(lx, ly, 1)
    poly = np.poly1d(coeffs)
    yf = yfit(x)
    
    return x, yf, coeffs


def plot_rain_spectra(mak):
    """Assuming precip and rain_ta 
    """

    fs = 1.0 # 1.0/dt
    nwr = 8.0
    m_freqr, m_rain_e, m_rain_ta_e = calc_ns_spectra_python(mak.precip.values, mak.rain_ta.values, fs, nwr)

    print("Welch window {:.0f} days".format(mak.shape[0]/nwr/24.))

    mcol = 'C0'
    # wcol = 'g'
    lcol = 'k'
    # hlength = 1.0
    # rc('font', size=13)
    # rc('legend', fontsize=14)
    # lbox = (0.7, 0.5)
    # style = dict(size=20, color='k')
    rain_fit_limit = 1/(24.*1)
    # xlab = "Frequency "+r"$(h^{-1})$"


    fig, ax = plt.subplots(1, figsize=(17, 15))
    # ax = ax.flatten()
    x, y, c = calc_log_fit(m_freqr, m_rain_e, rain_fit_limit)
    Rlab = r"$f^{:.2f}$".format(c[0])
    ax.loglog(m_freqr, m_rain_e, '-', c=mcol)
    ax.loglog(x, y, '-', label=Rlab, c=lcol)
    ax.set(ylabel=r"$E_{np}(f)$")
    ax.legend(bbox_to_anchor=(0.82, 0.9), frameon=False)

    # lf.set_vert_lines(ax)
    # lf.set_minor_ticks_off(ax)
    # # Remove top and right axis
    # for i in range(9):
    #     ax[i].spines['right'].set_visible(False)
    #     ax[i].get_yaxis().tick_left()
    #     ax[i].spines['top'].set_visible(False)
    #     ax[i].get_xaxis().tick_bottom()
    #     ax[i].set_xticks([1/8760., 1/720., 1/24., 1/12.])
    #     xlabels = ["1 yr", "1 month", "24h", "    12h"]
    #     ax[i].set_xticklabels(xlabels)
    #     ax[i].minorticks_off()
    # for i in range(3):
    #     ax[i].set_ylim(0.05, 30)
    # for i in range(3,9):
    #     ax[i].set_ylim(2e-6, 1e4)
    # for i in range(6,9):
    #     ax[i].set(xlabel=xlab)    
    # fig.subplots_adjust(wspace=0.6)
    # fig.subplots_adjust(hspace=0.2)

def set_vert_lines(ax):
    for i in ax:
        i.axvline(1/12.,ls='--', alpha=0.5, color='k')
        i.axvline(1/24.,ls='--', alpha=0.5, color='k')
        i.axvline(1/720.,ls='--', alpha=0.5, color='k')
        i.axvline(1/8760.,ls='--', alpha=0.5, color='k')

def set_minor_ticks_off(ax):
    for i in ax:
        i.xaxis.set_minor_locator(plt.NullLocator())
        i.yaxis.set_minor_locator(plt.NullLocator())


def plot_all(wel, params_wel, smod1_w, smod2_w, prefix='../../images/new/'):
    """
    df and two models - 1 and 2 

    """

    dt = 1.0
    fs = 1.0 # 1.0/dt
    dtn = 1.0

    iformat = '.png'
    title2 = ''
    tau_s_w = calc_integral_scale_py(wel.s.values)*dtn  # in hours
    tau_s_w1 = calc_integral_scale_py(smod1_w)*dtn # in hours
    # tau_s_w2 = 744.1491 # measured
    tau_s_w2 = calc_integral_scale_py(smod2_w)*dtn # in hours

    s_ta_w = calc_ta_stress(wel.s, params_wel['s_stress'])
    s_ta_w1 = calc_ta_stress(smod1_w, params_wel['s_stress'])
    s_ta_w2 = calc_ta_stress(smod2_w, params_wel['s_stress'])

    time_w = np.arange(wel.s.shape[0]) * dt

    assert np.sum(np.isnan(wel.s.values)) == 0
    assert np.sum(np.isnan(smod1_w)) == 0
    assert np.sum(np.isnan(smod2_w)) == 0

    
    # Time series
    rc('font', size=13)
    rc('legend', fontsize=14)
    colm = 'C3' # Measured
    colm1 = 'C5' # Model 1
    colm2 = 'C2' # Model 2
    fig, ax = plt.subplots(1, figsize=(10,8))
    ax.plot_date(wel.index.values, smod1_w, '-', c=colm1, label='Model 1')
    ax.plot_date(wel.index.values, smod2_w, '-', c=colm2, label='Model 2')
    ax.plot_date(wel.index.values,wel.s.values, '-', c=colm, label='Measured')
    ax.set(title=title2)
    ax.margins(x=0)
    ax.legend(frameon=False)
    ax.set(ylabel="s[%]", ylim=(0,1))
    plt.savefig(prefix+'fig2'+iformat, bbox_inches='tight')


    nw = 8.0
    nwr = 8.0

    w_freq, w_s_e, w_s_ta_e = calc_ns_spectra_python(wel.s.values, s_ta_w.values, fs, nw)
    w_freq1, w_s_e1, w_s_ta_e1 = calc_ns_spectra_python(smod1_w, s_ta_w1, fs, nw)
    w_freq2, w_s_e2, w_s_ta_e2 = calc_ns_spectra_python(smod2_w, s_ta_w2, fs, nw)
    w_freqr, w_rain_e, w_rain_ta_e = calc_ns_spectra_python(wel.precip.values, wel.rain_ta.values, fs, nwr)

    print("Welgegund welch window {:.0f} days".format(wel.s.shape[0]/nw/24.))


    def fit_label(txt, freq, e, tau):
        x, y, c = calc_log_fit(freq, e, tau)    
        # lab = txt+" "+r"($f^{{{0:.2f}}}$)".format(c[0])
        lab = ""
        if txt=="Meas":
            lab = "$f^{{{0:.2f}}}_{{M}}$,".format(c[0])+" "+r"$\tau={{{0:.0f}}}$ d".format(1/tau/24.0)
        elif txt=="M1":
            lab = "$f^{{{0:.2f}}}_{{M1}}$,".format(c[0])+" "+r"$\tau={{{0:.0f}}}$ d".format(1/tau/24.0)
        elif txt=="M2":
            lab = "$f^{{{0:.2f}}}_{{M2}}$,".format(c[0])+" "+r"$\tau={{{0:.0f}}}$ d".format(1/tau/24.0)
        return x, y, c, lab
    def fit_ta_label(txt, freq, e, tau):
        lab =""
        x, y, c = calc_log_fit(freq, e, tau)    
        if txt=="Meas":
            lab = "$f^{{{0:.2f}}}_{{M}}$".format(c[0])
        elif txt=="M1":
            lab = "$f^{{{0:.2f}}}_{{M1}}$".format(c[0])
        elif txt=="M2":
            lab = "$f^{{{0:.2f}}}_{{M2}}$".format(c[0])
        return x, y, c, lab
    mcol = 'C0'
    wcol = 'g'
    lcol = 'k'
    hlength = 1.0
    rc('font', size=13)
    rc('legend', fontsize=14)
    lbox = (0.7, 0.5)
    style = dict(size=20, color='k')
    rain_fit_limit = 1/(24.*1)
    xlab = "Frequency "+r"$(h^{-1})$"
    fig, ax = plt.subplots(3,1, figsize=(10, 12))
    ax = ax.flatten()
    x, y, c = calc_log_fit(w_freqr, w_rain_e, rain_fit_limit)
    Rlab2 = r"$f^{:.2f}$".format(c[0])
    ax[0].loglog(w_freqr, w_rain_e, '-', c=mcol)
    ax[0].loglog(x, y, '-', label=Rlab2, c=lcol)
    ax[0].set(title=title2)
    ax[0].legend(bbox_to_anchor=(0.82, 0.9), frameon=False)

    # Soil moisture
    x, y, c, lab = fit_label('Meas',w_freq, w_s_e, 1/tau_s_w)
    ax[1].loglog(w_freq, w_s_e, '-', c=colm)
    ax[1].loglog(x, y, '-', label=lab,c=colm)
    x, y, c, lab = fit_label('M1',w_freq1, w_s_e1, 1/tau_s_w1)
    ax[1].loglog(w_freq1, w_s_e1, '-', c=colm1)
    ax[1].loglog(x, y, '-', label=lab, c=colm1)
    x, y, c, lab = fit_label('M2',w_freq2, w_s_e2, 1/tau_s_w2)
    ax[1].loglog(w_freq2, w_s_e2, '-', c=colm2)
    ax[1].loglog(x, y, '-', label=lab, c=colm2)
    ax[1].axvline(1/tau_s_w,ls='-', alpha=0.9, color=colm) # Memory
    ax[1].axvline(1/tau_s_w1,ls='-', alpha=0.9, color=colm1) 
    ax[1].axvline(1/tau_s_w2,ls='-', alpha=0.9, color=colm2)
    ax[1].legend(bbox_to_anchor=lbox, frameon=False, handlelength=hlength)
    # Soil moisture TA
    # x, y, c = lf.calc_log_fit(w_freq, w_s_ta_e, 1/tau_s_w)
    x, y, c, lab= fit_ta_label("Meas", w_freq, w_s_ta_e, 1/tau_s_w)
    ax[2].loglog(w_freq, w_s_ta_e, '-', c=colm)
    ax[2].loglog(x, y, '-', label=lab, c=colm)
    x, y, c, lab= fit_ta_label("M1", w_freq1, w_s_ta_e1, 1/tau_s_w1)
    ax[2].loglog(w_freq1, w_s_ta_e1, '-', c=colm1)
    ax[2].loglog(x, y, '-', label=lab, c=colm1)
    x, y, c, lab= fit_ta_label("M2", w_freq2, w_s_ta_e2, 1/tau_s_w2)
    ax[2].loglog(w_freq2, w_s_ta_e2, '-', c=colm2)
    ax[2].loglog(x, y, '-', label=lab, c=colm2)
    # ax[7].text(0.005, 13, r"$f^{{{0:.2f}}}$".format(c[0]),**style)
    ax[2].legend(bbox_to_anchor=lbox, frameon=False, handlelength=hlength)
    ax[2].axvline(1/tau_s_w,ls='-', alpha=0.9, color=colm) # Memory
    ax[2].axvline(1/tau_s_w1,ls='-', alpha=0.9, color=colm1) # Memory
    ax[2].axvline(1/tau_s_w2,ls='-', alpha=0.9, color=colm2) # Memory
    set_vert_lines(ax)
    set_minor_ticks_off(ax)
    # Remove top and right axis
    for i in range(3):
        ax[i].spines['right'].set_visible(False)
        ax[i].get_yaxis().tick_left()
        ax[i].spines['top'].set_visible(False)
        ax[i].get_xaxis().tick_bottom()
        ax[i].set_xticks([1/8760., 1/720., 1/24., 1/12.])
        xlabels = ["1 yr", "1 month", "24h", "    12h"]
        ax[i].set_xticklabels(xlabels)
        ax[i].minorticks_off()
    # for i in range(3):
    #     ax[i].set_ylim(0.05, 30)
    # for i in range(3,9):
    #     ax[i].set_ylim(2e-6, 1e4)
    # for i in range(6,9):
    #     ax[i].set(xlabel=xlab)    
    fig.subplots_adjust(wspace=0.6)
    # fig.subplots_adjust(hspace=0.2)
    plt.savefig(prefix+'fig3'+iformat, bbox_inches='tight')
    
    #########
    # Fig 3 #
    #########

    flag_lstyle = False

    if flag_lstyle:
        lstyle = "-"
    else:
        lstyle = "None"

    # print("\n")
    # print("s* = {}".format(params_mak['s_stress']))
    # print(tau_s_m1, tau_s_m2)
    lwip = 2
    fig, ax = plt.subplots(1, figsize=(10,10))
    plot_dry_ip_khaled(ax, time_w, s_ta_w.values, tau_s_w, 'Measured', color=colm)
    plot_dry_ip_khaled(ax, time_w, s_ta_w1, tau_s_w1, 'Model 1', color=colm1)
    plot_dry_ip_khaled(ax, time_w, s_ta_w2, tau_s_w2, 'Model 2', color=colm2)
    ax.legend(loc=3, frameon=False)
    # ax.set(title=title2)
    ax.axvline(1.0,ls='-', alpha=0.5, color='k')    
    ax.spines['right'].set_visible(False)
    ax.get_yaxis().tick_left()
    ax.spines['top'].set_visible(False)
    ax.get_xaxis().tick_bottom()
    ax.set_xlim(0.0007, 30)
    ax.set_ylim(0.001, 140)
    ax.set_ylabel(r"$PDF(I_{dry}/\tau)$")    
    ax.xaxis.set_major_formatter(ticker.StrMethodFormatter("{x}"))    
    fig.subplots_adjust(hspace=0.3)
    plt.savefig(prefix+'fig4'+iformat, bbox_inches='tight')


    poff = calc_off_times(s_ta_w.values).values.squeeze(axis=1)
    poff1 = calc_off_times(s_ta_w1).values.squeeze(axis=1)
    poff2 = calc_off_times(s_ta_w2).values.squeeze(axis=1) 
    # print(poff.max()/ tau_s_w2, poff2.max()/ tau_s_w2)
    
    print("Measured avg dry time (T) = {:.1f} days, M1 = {:.1f} days, M2 = {:.1f} days".format(poff.mean()/24.,
                                                                             poff1.mean()/24.,
                                                                             poff2.mean()/24.))

def calc_off_times(ser):
    "Waiting times"
    ser = pd.Series(ser)
    ff = (ser.shift(1) != ser).astype(int).cumsum()
    ff[ser > 0.0] = np.nan
    ff = pd.DataFrame(ff)
    ff['enum'] = ff.iloc[:,0].values
    offt = ff.groupby('enum').count()
    return offt


# def plot_dry_ip_khaled(ax, time, s_ta, tau_s, label, color, lstyle="None"):

#     poff = calc_off_times(s_ta).values.squeeze(axis=1) / tau_s
#     # print("Number of dry times {} - ".format(poff.shape[0])+label)
#     # if label=="Measured":
#     print("{}: longest dry time {:.3f} ".format(label, poff.max()))

#     # NOTICE: Ignore Runtime Warn
#     with warnings.catch_warnings():
#         warnings.simplefilter("ignore")    
#         fit_po = mp.Fit(poff, xmin=poff.min())

        
#     # print(fit_po.my_exp.parameter1)
#     axip = fit_po.plot_pdf(ax=ax, color=color, marker='.', linestyle=lstyle)
#     labeltxt = label+" "+r"$(\beta = {{{:.2f}}})$".format(fit_po.my_exp.beta)
#     fit_po.my_exp.plot_pdf(color=color, linestyle='--', ax=axip, label=labeltxt)


def get_dry_beta(s_ta, tau_s):
    """Return beta and max dry period
    """
    poff = calc_off_times(s_ta).values.squeeze(axis=1) / (tau_s*1.0)
    # print("Number of dry times {} - ".format(poff.shape[0])+label)
    # if label=="Measured":
    # print("{}: longest dry time {:.3f} ".format(label, poff.max()))

    # NOTICE: Ignore Runtime Warn
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")    
    fit_po = mp.Fit(poff, xmin=poff.min())

    beta = fit_po.my_exp.beta
    pmax = poff.max()
     
    return beta, pmax


def calc_dry_ip_khaled(s_ta, tau_s):
    """Plot interpulse period pdf like in Ghannam fig. 7
    """
    poff = calc_off_times(s_ta).values.squeeze(axis=1) / (tau_s*1.0)

    # NOTICE: Ignore Runtime Warn
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")    
    fit_po = mp.Fit(poff, xmin=poff.min())

    return fit_po.my_exp.beta


def plot_dry_ip_khaled(ax, time, s_ta, tau_s, label, color, lstyle="None"):
    """Plot interpulse period pdf like in Ghannam fig. 7
    """
    poff = calc_off_times(s_ta).values.squeeze(axis=1) / (tau_s*1.0)
    # print("Number of dry times {} - ".format(poff.shape[0])+label)
    # if label=="Measured":
    print("{}: longest dry time {:.3f} ".format(label, poff.max()))

    # NOTICE: Ignore Runtime Warn
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")    
    fit_po = mp.Fit(poff, xmin=poff.min())


    # print(fit_po.my_exp.parameter1)
    axip = fit_po.plot_pdf(ax=ax, color=color, marker='.', linestyle=lstyle)
    labeltxt = label+" "+r"$(\beta = {{{:.2f}}})$".format(fit_po.my_exp.beta)
    fit_po.my_exp.plot_pdf(color=color, linestyle='--', ax=axip, label=labeltxt)
    return poff

def plot_ts(wel, smod0_w, smod1_w, smod2_w):
    """Plot s time series and their residuals
    """
    dif0 = pd.DataFrame(wel.s.values-smod0_w)
    dif0 = dif0.set_index(wel.index)
    dif = pd.DataFrame(wel.s.values-smod1_w)
    dif = dif.set_index(wel.index)
    dif2 = pd.DataFrame(wel.s.values-smod2_w)
    dif2 = dif2.set_index(wel.index)
    pre = wel.precip.copy()
    pre.loc[pre > 1.0] = 0.0
    rc('font', size=13)
    rc('legend', fontsize=14)
    colm = 'C3' # Measured
    colm1 = 'C5' # Model 1
    colm2 = 'C2' # Model 2
    fig, ax = plt.subplots(2, figsize=(10,10), sharex=True)
    ax[0].plot_date(wel.index.values,wel.s.values, '-', c=colm, label='Measured')
    ax[0].plot_date(wel.index.values, smod0_w, '-', c='C1', label='Model 0')
    ax[0].plot_date(wel.index.values, smod1_w, '-', c='k', label='Model 1')
    ax[0].plot_date(wel.index.values, smod2_w, '-', c='C8', label='Model 2')
    ax2 = ax[0].twinx()
    ax2.plot_date(wel.index.values, wel.ndvi.values, '-', c='gray', label='NDVI')
    ax2.set(ylabel='NDVI')
    ax[1].plot_date(dif0.index.values, dif0.values, '-', c='C1', label='Model 0')
    ax[1].plot_date(dif.index.values, dif.values, '-', c='k', label='Model 1')
    ax[1].plot_date(dif2.index.values, dif2.values, '-', c='C8', label='Model 2')
    # ax[1].plot_date(dif0.rolling('7D').mean().index.values, dif0.rolling('7D').mean(), '-', c='C1', label='Model 0')
    # ax[1].plot_date(dif.rolling('7D').mean().index.values, dif.rolling('7D').mean(), '-', c='k', label='Model 1')
    # ax[1].plot_date(dif2.rolling('7D').mean().index.values, dif2.rolling('7D').mean(), '-', c='C8', label='Model 2')
    for i in range(2):
        ax[i].margins(x=0)
        ax[i].legend(frameon=False)
        ax[i].xaxis.set_major_locator(mdates.MonthLocator(bymonth=9))
    for y in wel.index.year.unique()[:-1]:    
        ax[1].axvline(pd.to_datetime(str(y)+'0901'), ls='--', alpha=0.3, c='k')
        ax[0].axvline(pd.to_datetime(str(y)+'0901'), ls='--', alpha=0.3, c='k')
    ax[0].set(ylabel="s[%]", ylim=(0,1))
    ax[1].axhline(0, ls='--', alpha=0.3)
    ax[1].set(ylabel='Measured - Model')
    plt.savefig("Grazed_savanna_Model3.png")

def plot_s_vs_dry_time(fig, ax, x, y, site='', loc="upper right"):
    """Plot x vs y correlation plot with fit statistics"""

    xname = 'S'
    yname = r'$T_{dry}$'
    assert x.shape[0] == y.shape[0]

    cond = x.isnull() | y.isnull()
    x = x[~cond]
    y = y[~cond]

    assert x.isnull().sum() == 0
    assert y.isnull().sum() == 0

    fit = np.polyfit(x, y, deg=1)
    r2 = r2_score(y, fit[0] * x + fit[1])
    prediction = fit[0] * x + fit[1]

    # fig, ax = plt.subplots(figsize=(8,8))
    ax.scatter(x, y, color="k", alpha=0.8)
    if site =='grazed':
        x = np.arange(0.27, 0.8, 0.05)
        ax.plot(x, fit[0] * x + fit[1], color="k")
    else:
        ax.plot(x, fit[0] * x + fit[1], color="k")
    ax.set_xlabel('Rainfall seasonality index')
    ax.set_ylabel('Model 1 total dry time [days/year]')
    # ax.axhline(y=0, ls='--', color='0.5')
    text = (
        yname
        + " = {:0.4f} * ".format(fit[0])
        + xname
        + " + {:1.2f} \nR2 = {:2.2f}".format(fit[1], r2)
    )
    anchored_text = AnchoredText(text, loc=loc)
    ax.add_artist(anchored_text)
